#include <sys/socket.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <errno.h>
#include <string.h>
#include <time.h>

// map ip protocol numbers to strings
char *IP_PROTO_TYPE[] = {
    "", "icmp", "igmp", "", "", "", "tcp", "", "", "", "", "", "", "", "", "", "", "udp"
};

// map icmp types to string
char *ICMP_TYPE[] = {
    "echo reply", "", "",
    "destination unreachable", "",
    "redirect", "", "",
    "echo", "ra", "rs", "time exceeded"
};

// 20 bytes ip header
struct IPv4 {
	uint8_t vers_ihl;
	uint8_t tos;
	uint16_t pkt_len;
	uint16_t id;
	uint16_t flags;
	uint8_t ttl;
	uint8_t protocol;
	uint16_t checksum;
	struct in_addr src_ip;
	struct in_addr dst_ip;
};

int16_t calc_checksum(const uint16_t *data, unsigned int bytes) {
	uint32_t sum = 0;
	for (unsigned int i = 0; i < bytes / 2; i++) {
		sum += data[i];
	}
	sum = (sum & 0xffff) + (sum >> 16);
	return htons(0xffff - sum);
}

void check_args(int argc, char *argv[]) {
	if (argc < 3) {
		printf("Invalid parameters.\n");
		printf("- Usage %s <source IP> <target IP> \n", argv[0]);
		exit(-1);
	}
}

// Open raw socket, set options and return the file descriptor
int open_socket() {
    // open a raw socket for sending and receiving
	int rawsock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (rawsock == -1) {
		printf("error socket(): %s\n", strerror(errno));
	}
    printf("Opened raw socket. FD: %d\n", rawsock);

	// set socket options on raw socket
    // IP_HDRINCL: we will include the ip header ourself. stop the kernel from adding it
	const int one = 1;
	ssize_t ret = setsockopt(rawsock, IPPROTO_IP, IP_HDRINCL, (char *) &one, sizeof(one));
	if (ret == -1) {
		printf("error setsockopt(): %s\n", strerror(errno));
	}
	return rawsock;
}

// create our own IP header and store it in buf
void create_ip_header(char *buf, struct in_addr *src_ip, struct in_addr *dst_ip, int ttl) {
	struct IPv4 *ip_pkt = (struct IPv4 *) buf;

	ip_pkt->vers_ihl = 0x45; // set version to 4 and IP header length to 5 * 4 byte = 20 byte
	ip_pkt->tos = 0; // type of service 0
	ip_pkt->pkt_len = sizeof(struct IPv4) + sizeof(struct icmphdr); // total length = 20 byte ip header + 8 byte icmp
	ip_pkt->id = htons(10000); // arbitrary packet id
	ip_pkt->flags = htons(0x4000); // set DF bit in flags
	ip_pkt->ttl = ttl; // this will be incremented
	ip_pkt->protocol = IPPROTO_ICMP; // protocol: 1 (ICMP)
	ip_pkt->checksum = 0; // set to 0, calculate at the end
	ip_pkt->src_ip = *src_ip;
	ip_pkt->dst_ip = *dst_ip;
	ip_pkt->checksum = htons(calc_checksum((uint16_t*) &ip_pkt,
				sizeof(struct IPv4)));
}

// create icmp echo header and store it in buf, after IP header
void create_icmp_header(char *buf, int ttl) {
    // pointer to send-buffer, after the IP header (buf + 20)
	struct icmphdr *icmphd = (struct icmphdr *) (buf + sizeof(struct IPv4));
	icmphd->type = ICMP_ECHO; // type: 8 (ECHO REQUEST)
	icmphd->code = 0;
	icmphd->checksum = 0;
	icmphd->un.echo.id = 0; // echo datagram id
	icmphd->un.echo.sequence = ttl; // echo datagram seq. nr
	icmphd->checksum = htons(calc_checksum((uint16_t*) (buf + 20), 8));
}

// print some info about the sent ip packet
void print_sent_packet(char *buf) {
    struct IPv4 *ip_hdr = (struct IPv4*) buf;
    struct icmphdr *icmp_hdr = (struct icmphdr*) (buf + sizeof(struct IPv4));

	// parse ip header
    //uint16_t ip_len = ntohs(ip_hdr->pkt_len);
    //uint16_t ip_id = ntohs(ip_hdr->id);
    uint8_t ip_ttl = ip_hdr->ttl;
    uint8_t ip_proto = ip_hdr->protocol;
    //char ip_src[INET_ADDRSTRLEN];
    //char ip_dst[INET_ADDRSTRLEN];
    //inet_ntop(AF_INET, &ip_hdr->src_ip, ip_src, INET_ADDRSTRLEN);
    //inet_ntop(AF_INET, &ip_hdr->dst_ip, ip_dst, INET_ADDRSTRLEN);

	// parse icmp header
    uint8_t icmp_type = icmp_hdr->type;
    //uint8_t icmp_code = icmp_hdr->code;
    //uint16_t icmp_checksum = ntohs(icmp_hdr->checksum);

	printf("Sent packet. IP_TTL: %d, IP_PROTO: %d (%s), ICMP_TYPE: %d (%s)\n",
			ip_ttl, ip_proto, IP_PROTO_TYPE[ip_proto], icmp_type, ICMP_TYPE[icmp_type]);
}

// parse and print the received packet. return the icmp type
int parse_received_packet(char *buf, struct sockaddr_in *sa) {
    struct icmphdr *icmp_hdr = (struct icmphdr*) (buf + sizeof(struct IPv4));

	// get icmp type
    uint8_t icmp_type = icmp_hdr->type;

	// get source addr
	char rcv_adr[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &sa->sin_addr.s_addr, rcv_adr, INET_ADDRSTRLEN);
	printf("Received packet. ICMP_TYPE: %d (%s), src: %s\n\n", icmp_type, ICMP_TYPE[icmp_type], rcv_adr);

	return (int) icmp_type;
}

// sleep for given amount of miliseconds
void msleep(int msec) {
    struct timespec tim;
    tim.tv_sec = 0;
    tim.tv_nsec = msec * 10000L;
	nanosleep(&tim, NULL);
}

int main(int argc, char *argv[]) {
	check_args(argc, argv);
	ssize_t ret;                    // return code
	char snd_buf[1024];             // send buffer
	char rcv_buf[1024];             // receive buffer
	int ttl = 1;					// start ttl at 1

	// open socket
	int rawsock = open_socket();

    // src and dst addresses from args
	struct in_addr src_ip;
    char *src_ip_str = argv[1];
	ret = inet_pton(AF_INET, src_ip_str, &src_ip);
	if (ret == -1) {
		printf("error src inet_pton(): %s\n", strerror(errno));
	}
    printf("Set source address: %s\n", src_ip_str);

	struct in_addr dst_ip;
    char *dst_ip_str = argv[2];
	ret = inet_pton(AF_INET, dst_ip_str, &dst_ip);
	if (ret == -1) {
		printf("error dst inet_pton(): %s\n", strerror(errno));
	}
    printf("Set destination address: %s\n\n", dst_ip_str);

	// destination for sendto()
	struct sockaddr_in dst;
	memset(&dst, 0, sizeof(dst));
	dst.sin_family = AF_INET;
	dst.sin_addr = dst_ip;
	dst.sin_port = htons(12345);

	// increment ttl and send icmp echoes
	for (ttl = 1; ttl < 20; ttl++) {
		// create ip header
		create_ip_header(snd_buf, &src_ip, &dst_ip, ttl);
		// create icmp echo header
		create_icmp_header(snd_buf, ttl);

    	// send request on socket
		ret = sendto(rawsock, snd_buf, (sizeof(struct IPv4) + sizeof(struct icmphdr)), 0, (struct sockaddr *) &dst, sizeof(dst));
		if (ret == -1) {
			printf("error sendto(): %s\n", strerror(errno));
		}
		print_sent_packet(snd_buf);

    	struct sockaddr_in saddr;
    	int saddr_len = sizeof(saddr);
		// read echo reply from socket
    	ret = recvfrom(rawsock, rcv_buf, 4096, 0, (struct sockaddr *) &saddr, (socklen_t *) &saddr_len);
		if (ret == -1) {
			printf("error recvfrom(icmp): %s\n", strerror(errno));
		}
		// parse the response. if we get an echo reply, we are done
		int icmp_type = parse_received_packet(rcv_buf, &saddr);
		if (icmp_type == 0) {
			puts("Destination reached. Exiting.");
			break;
		}
		// clear send and receive buffer
		// memset(snd_buf, 0, sizeof(snd_buf));
		// memset(rcv_buf, 0, sizeof(rcv_buf));

        msleep(250);
	}
	exit(0);
}

