#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>

void usage()
{
	printf("EXAMPLE USAGE:\n");
	printf("recv <filename> <server-ip> <server-port>\n");
}

int main(int argc, char **argv)
{
        int client_socket;
        ssize_t len;
        ssize_t ret;
        struct sockaddr_in remote_addr;
        char buffer[BUFSIZ];
        int file_size;
        FILE *received_file;
        int remain_data = 0;

		if (argc != 4) {
			printf("Bad args\n");
			usage();
			exit(1);
		}

		char *filename = argv[1];

		struct in_addr dst_ip;
    	char *dst_ip_str = argv[2];
        ret = inet_pton(AF_INET, dst_ip_str, &dst_ip);
        if (ret == -1) {
                printf("error dst inet_pton(): %s\n", strerror(errno));
        }
    	printf("Set destination address: %s\n\n", dst_ip_str);

		int port = atoi(argv[3]);

        /* Zeroing remote_addr struct */
        memset(&remote_addr, 0, sizeof(remote_addr));

        /* Construct remote_addr struct */
        remote_addr.sin_family = AF_INET;
		remote_addr.sin_addr = dst_ip;
        remote_addr.sin_port = htons(port);

        /* Create client socket */
        client_socket = socket(AF_INET, SOCK_STREAM, 0);
        if (client_socket == -1) {
                fprintf(stderr, "Error creating socket --> %s\n", strerror(errno));
                exit(EXIT_FAILURE);
        }

        /* Connect to the server */
        if (connect(client_socket, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr)) == -1) {
                fprintf(stderr, "Error on connect --> %s\n", strerror(errno));
                exit(EXIT_FAILURE);
        }

        /* Receiving file size */
        recv(client_socket, buffer, BUFSIZ, 0);
        file_size = atoi(buffer);
        //fprintf(stdout, "\nFile size : %d\n", file_size);

        received_file = fopen(filename, "w");
        if (received_file == NULL)
        {
                fprintf(stderr, "Failed to open file foo --> %s\n", strerror(errno));
                exit(EXIT_FAILURE);
        }

        remain_data = file_size;

        while (((len = recv(client_socket, buffer, BUFSIZ, 0)) > 0) && (remain_data > 0))
        {
                fwrite(buffer, sizeof(char), len, received_file);
                remain_data -= len;
                fprintf(stdout, "Receive %d bytes and we hope :- %d bytes\n", (int) len, (int) remain_data);
        }
        fclose(received_file);

        close(client_socket);

        return 0;
}
