#include <stdio.h>
#include <pcap/pcap.h>
#include <stdlib.h>
#include <netinet/ip.h>

#define ETHER_ADDR_LEN 6	/* ethernet address size */
#define SIZE_ETHERNET 14	/* ethernet header size */
#define IP_RF 0x8000            /* reserved fragment flag */
#define IP_DF 0x4000            /* dont fragment flag */
#define IP_MF 0x2000            /* more fragments flag */
#define IP_OFFMASK 0x1fff       /* mask for fragmenting bits */
#define IP_HL(ip)               (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)                (((ip)->ip_vhl) >> 4)
#define TH_OFF(th)      (((th)->th_offx2 & 0xf0) >> 4)

/* values for TCP flags */
#define TH_FIN 0x01
#define TH_SYN 0x02
#define TH_RST 0x04
#define TH_PUSH 0x08
#define TH_ACK 0x10
#define TH_URG 0x20
#define TH_ECE 0x40
#define TH_CWR 0x80
#define TH_FLAGS (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)

struct EthernetHeader {
    u_char ether_dhost[ETHER_ADDR_LEN]; /* destination host */
    u_char ether_shost[ETHER_ADDR_LEN]; /* source host */
    u_short ether_type; /* ip? arp? etc */
};

struct ipheader {
    u_char ip_vhl;              /* version << 4 | header length >> 2 */
    u_char ip_tos;              /* type of service */
    u_short ip_len;             /* total length */
    u_short ip_id;              /* identification */
    u_short ip_off;             /* fragment offset field */
    u_char ip_ttl;              /* time to live */
    u_char ip_p;                /* protocol */
    u_short ip_sum;             /* checksum */
    struct in_addr ip_src, ip_dst; /* source and dest address */
};

struct TCPHeader {
    u_short th_sport;   /* source port */
    u_short th_dport;   /* destination port */
    u_int th_seq;	/* sequence number */
    u_int th_ack;	/* acknowledgement number */
    u_char th_offx2;    /* data offset, rsvd */
    u_char th_flags;	/* flags */
    u_short th_win;     /* window */
    u_short th_sum;     /* checksum */
    u_short th_urp;     /* urgent pointer */
};

/* flag counters for statistics */
uint fin = 0;
uint syn = 0;
uint rst = 0;
uint ack = 0;

void packet_handler(u_char *user, const struct pcap_pkthdr *header, const u_char *packet)
{
    printf("Got packet\n");
    /* const struct EthernetHeader *ethernet; */
    const struct ipheader *ip;
    const struct TCPHeader *tcp;
    /* const char *payload; */

    u_int size_tcp;

    /* ethernet = (const struct EthernetHeader*)(packet); */
    ip = (const struct ipheader*)(packet + SIZE_ETHERNET);
    u_int size_ip = IP_HL(ip) * 4;
    if(size_ip < 20) {
        printf("Invalid IP header length\n");
        return;
    }
    tcp = (struct TCPHeader*)(packet + SIZE_ETHERNET + size_ip);
    size_tcp = TH_OFF(tcp)*4;
    if(size_tcp < 20)
    {
        printf("Invalid TCP header length\n");
        return;
    }
    /* payload = (char *)(packet + SIZE_ETHERNET + size_ip + size_tcp); */
    printf("Packet seems ok\n");
    printf("raw flags: %#010x\n", tcp->th_flags);
    if (0 != (TH_FIN & tcp->th_flags))
        fin++;
    if (0 != (TH_SYN & tcp->th_flags))
        syn++;
    if (0 != (TH_RST & tcp->th_flags))
        rst++;
    if (0 != (TH_ACK & tcp->th_flags))
        ack++;
    /* and so on and so forth */
}

int main(int argc, char *argv[])
{
    bpf_u_int32 mask; /* netmask */
    bpf_u_int32 net; /* network */

    pcap_t *handle;
    char errbuf[PCAP_ERRBUF_SIZE];

    if (pcap_lookupnet(argv[1], &net, &mask, errbuf) == -1)
    {
        net = 0;
        mask = 0;
    }

    handle = pcap_open_live(argv[1], 1500, 1, 0, errbuf);


    /* filter */
    struct bpf_program fp;
    /* const char *filter_exp = "tcp[tcpflags] & (tcp-fin|tcp-ack|tcp-syn|tcp-rst) == (tcp-ack|tcp-rst) and (src port 22)"; */
    const char *filter_exp = "";
    if(pcap_compile(handle, &fp, filter_exp, 0, net) == -1)
    {
        printf("Filter expression invalid\n");
        return 1;
    }

    if(pcap_setfilter(handle, &fp) == -1)
    {
        printf("Filter set error\n");
        return 1;
    }
    printf("Starting loop\n");
    pcap_loop(handle, atoi(argv[2]), &packet_handler, NULL);
    printf("Loop ended\n");
    printf("Stats: \nfin: %d syn: %d rst: %d: ack: %d\n", fin, syn, rst, ack);
}
