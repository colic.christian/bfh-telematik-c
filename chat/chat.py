#!/usr/bin/env python3

from socket import *
from select import select
import sys
import datetime

USER = input("Your username: ")
PORT = int(input("Listen port: "))


def run():
    # sending socket. enable broadcast
    sender = socket(AF_INET, SOCK_DGRAM)
    sender.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    sender.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

    # receiving socket, listen on any ip
    receiver = socket(AF_INET, SOCK_DGRAM)
    receiver.bind(('0.0.0.0', PORT))

    # send to broadcast that we have now joined the chat
    sender.sendto(msg("'{}' has joined the chat".format(USER)), ("255.255.255.255", PORT))

    # list of our inputs. stdin and receiver socket
    input = [sys.stdin, receiver]
    running = 1
    while running:

        # select available input
        inputready, outputready, exceptready = select(input, [], [])

        for s in inputready:
            if s == receiver:
                # if input came from socket: print the received the msg
                data, addr = receiver.recvfrom(1024)
                print(data.decode())

            elif s == sys.stdin:
                # if input came from stdin, send that message to the chat
                message = sys.stdin.readline()
                if message.rstrip():
                    sender.sendto(msg("{}: {}".format(USER, message)), ("255.255.255.255", PORT))

    # close the sockets
    sender.close()
    receiver.close()


# Format our message with a timestamp
def msg(string):
    time = datetime.datetime.now().strftime('%H:%M:%S')
    return "{}: {}".format(time, string).encode()


if __name__ == '__main__':
    run()
