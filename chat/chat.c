#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <errno.h>


void print_line(int len, char *buf)
{
    time_t raw_time;
    time(&raw_time);
    char *timebuf = ctime(&raw_time);
    /* slice off newline of timestamp */
    printf("%.*s: %.*s", (int) strlen(timebuf)-1, timebuf, len, buf);
}

int main(int argc, char *argv[])
{
    const int LOCALPORT = 9999;

    int receiver = socket(AF_INET, SOCK_DGRAM, 0);

    /* enable non-blocking i/o */
    fcntl(receiver, F_SETFL, fcntl(receiver, F_GETFL) | O_NONBLOCK);

    /* enable broadcast */
    const int one = 1;
    setsockopt(receiver, SOL_SOCKET, SO_BROADCAST, (char *) &one, sizeof(one));

    /* bind to a port */
    struct sockaddr_in local;
    local.sin_family = AF_INET;
    local.sin_port = htons(LOCALPORT);
    local.sin_addr.s_addr = INADDR_ANY;
    bind(receiver, (struct sockaddr *) &local, sizeof(local));

    /*  send addr */
    struct in_addr dst_ip;
    char *dst_ip_str = "255.255.255.255";
    inet_pton(AF_INET, dst_ip_str, &dst_ip);
    struct sockaddr_in dst;
    memset(&dst, 0, sizeof(dst));
    dst.sin_family = AF_INET;
    dst.sin_addr = dst_ip;
    dst.sin_port = htons(9999);

    /* receive shit */
    char buf[65536];
    char stdin_buf[65536];
    int len;
    struct sockaddr_storage from;
    socklen_t slen = sizeof(from);

    /* send shit */
    /* timing shit */
    struct timespec sleeptime;
    sleeptime.tv_sec = (time_t) 0;
    sleeptime.tv_nsec = (long) 250*1000*1000;

    /* stdin nonblocking */
    fcntl(0, F_SETFL, fcntl(0, F_GETFL) | O_NONBLOCK);

    while(1)
    {
        /* read from stdin */
        len = read(0, stdin_buf, sizeof(stdin_buf));
        if(len > 0)
        {
            print_line(len, stdin_buf);
	    ssize_t ret = sendto(receiver, stdin_buf, len+1, 0, (struct sockaddr *) &dst, sizeof(dst));
	    if (ret == -1) {
		    printf("error sendto(): %s\n", strerror(errno));
	    }
        }
        /* get da msg */
        len = (int) recvfrom(receiver, buf, sizeof(buf), 0, (struct sockaddr *) &from, &slen);
        if(len > 0) /* got a msg */
        {
            print_line(len, buf);
        }
        else /* got nothing, wait and retry */
        {
            nanosleep(&sleeptime, NULL);
        }
    }
    return 0;
}

